//
// Created by sicco on 23/04/2021.
//

#include "predict.h"
#include <queue>

struct tail_state_compare{ bool operator()(const pair<long double, pair<apta_node*, tail*>> &a, const pair<long double, pair<apta_node*, tail*>> &b) const{ return a.first < b.first; } };

long double align_probability(state_merger* m, tail* t, bool always_follow){
    apta_node* n = m->aut->root;

    double long probability;

    priority_queue< pair<long double, pair<apta_node*, tail*>>,
            vector< pair<long double, pair<apta_node*, tail*>> >, tail_state_compare> Q;
    //set<pair<apta_node*, tail*>> V;

    map<apta_node*, int> V;
    state_set *states = m->get_all_states();

    Q.push(pair<long double,pair<apta_node*, tail*>>(log(1.0),
            pair<apta_node*, tail*>(n,t)));

    while(!Q.empty()){
        pair<long double, pair<apta_node*, tail*>> next = Q.top();
        Q.pop();

        probability = next.first;
        apta_node* next_node = next.second.first;
        tail* next_tail = next.second.second;

        //cerr << probability << " " << endl;

        if(next_tail == 0){
            return probability;
        }

        //if(probability < 0.1e-100) return probability;

        //if(V.find(next.second.second) != V.end()) continue;
        //V.insert(next.second.second);

        //cerr << probability << " " << next_node->number << " " << next_tail->get_index() << endl;

        if(next_tail->get_symbol() == -1){
            if(FINAL_PROBABILITIES)
                Q.push(pair<long double, pair<apta_node*, tail*>>(
                    probability + log(next_node->data->predict_prob(next_tail)),pair<apta_node*, tail*>(
                            pair<apta_node*, tail*>(next_node,0))));
            else
                Q.push(pair<long double, pair<apta_node*, tail*>>(
                        probability,pair<apta_node*, tail*>(
                                pair<apta_node*, tail*>(next_node,0))));
            /*
             * Q.push(pair<double, pair<apta_node*, tail*>>(
                    probability * next_node->data->predict_prob(next_tail),
                    pair<apta_node*, tail*>(next_node,0))); */
        } else {
            if(V.find(next_node) != V.end() && V[next_node] >= next_tail->get_index()) continue;
            V[next_node] = next_tail->get_index();

            apta_node* child = next_node->child(next_tail);
            if(child != 0){
                child = child->find();
                if(V.find(child) == V.end() || V[child] < next_tail->get_index() + 1) {
                    Q.push(pair<long double, pair<apta_node *, tail *>>(
                            probability + log(next_node->data->predict_prob(next_tail)),
                            pair<apta_node *, tail *>(child, next_tail->future())));
                }
                /*
                 * Q.push(pair<double, pair<apta_node*, tail*>>(
                        probability * next_node->data->predict_prob(next_tail),
                        pair<apta_node*, tail*>(child,next_tail->future()))); */
            }
            if(always_follow == false || child == 0) {

                for (state_set::iterator it = states->begin(); it != states->end(); ++it) {
                    apta_node *jump_child = *it;
                    if (jump_child != child && jump_child->child(next_tail) != 0) {
                        if(V.find(jump_child) == V.end() || V[jump_child] < next_tail->get_index()){
                            Q.push(pair<long double, pair<apta_node *, tail *>>(
                                    probability + log(next_node->data->predict_prob(0)),
                                    pair<apta_node *, tail *>(jump_child, next_tail)));
                        }
                        /*
                         * Q.push(pair<double, pair<apta_node *, tail *>>(
                                probability * next_node->data->predict_prob(0),
                                pair<apta_node *, tail *>(jump_child, next_tail->future()))); */
                    }
                }
            }
        }
    }
    return probability;
}

void predict(state_merger* m, ofstream& output){
    //cerr << "number, length, seq, sum_prob, product_prob, align_state_seq, align_prob, free_align_state_seq, free_align_prob, predict symbol, predict type" << endl;
    output << inputdata::get_size() << endl;
    for(int i = 0; i < inputdata::get_size(); ++i){
        cout << i << " ";

        tail* t = inputdata::get_tail(i);
        apta_node* n = m->aut->root;
        long double sum_prob = 0.0;
        long double prod_prob = 0.0;
        long double num_symbols = 0.0;

        string state_sequence = "[" + to_string(n->number);
        for(int j = 0; j < t->get_length(); j++){
            long double prob = n->data->predict_prob(t);
            sum_prob += prob;
            prod_prob += log(prob);
            num_symbols += 1.0;

            cout << inputdata::string_from_symbol(t->get_symbol()) << "." << prob;

            apta_node* child = n->child(t);
            if(child == 0){
                break;
            } else {
                n = child;
            }
            n = n->find();
            t = t->future();
            state_sequence = state_sequence + "," + to_string(n->number);
        }
        state_sequence = state_sequence + "]";

        if(FINAL_PROBABILITIES && t->get_symbol() == -1){
            long double prob = n->data->predict_prob(t);
            sum_prob += prob;
            prod_prob += log(prob);
            num_symbols += 1.0;
            cout << prob << " ";
        }
        cout << endl;

        output << prod_prob << endl;
        continue;

        long double aligned = align_probability(m, inputdata::get_tail(i), true);
        //pair<string, double> aligned2 = align_probability(m, inputdata::get_tail(i), false);

        int symb = n->data->predict_symbol();
        string symb_string = "end";
        if(symb != -1) symb_string = inputdata::alphabet[symb];
        string type_string = inputdata::types[n->data->predict_type()];

        //cerr << aligned << endl;

        //cerr << i
            //<< ", " << num_symbols
            //<< ", " << state_sequence
            //<< ", " << sum_prob
            //<< ", " << prod_prob;
            //<< ", " << aligned.first
            //<< ", " << aligned.second
            //<< ", " << aligned2.first
            //<< ", " << aligned2.second
            //<< ", " << symb_string
            //<< ", " << type_string << endl;
        output << aligned << endl;
    }
}
