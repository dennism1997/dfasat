//
// Created by sicco on 23/04/2021.
//
#ifndef FLEXFRINGE_PREDICT_H
#define FLEXFRINGE_PREDICT_H

#include "state_merger.h"
#include "inputdata.h"

void predict(state_merger* m, ofstream& output);

#endif //FLEXFRINGE_PREDICT_H
