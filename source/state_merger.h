
#ifndef _STATE_MERGER_H_
#define _STATE_MERGER_H_

#include <iostream>
#include <fstream>
#include <vector>
#include <set>
#include <list>
#include <map>
#include <unordered_map>
#include <string>

class state_merger;
//class merger_context;

#include "evaluate.h"
#include "apta.h"
#include "refinement.h"
#include "mem_store.h"

using namespace std;

/**
 * @brief The state merger. Whereas the 
 * 
 */
class state_merger{
private:
    // why are all functions below public?
    refinement* inconsistent_refinement;

public:
    //merger_context context;
    apta* aut;

    /* core of merge targets */
    state_set red_states;
    /* fringe of merge candidates */
    state_set blue_states;

    evaluation_function* eval;

    /* for building the apta  */
    map<string, int> seen;
    int node_number = 1;
    int num_merges = 0;

    /* recursive state merging routines */
    bool merge(apta_node* red, apta_node* blue);
    bool merge(apta_node* red, apta_node* blue, int depth, bool evaluate, bool perform, bool test);
    void merge_force(apta_node* red, apta_node* blue);
    bool merge_test(apta_node* red, apta_node* blue);
    void undo_merge(apta_node* red, apta_node* blue);

    /* recursive state splitting routines */
    bool split_single(apta_node* red, apta_node* blue, tail* t, bool evaluate);
    void undo_split_single(apta_node* red, apta_node* blue);
    bool split(apta_node* red, apta_node* blue);
    void undo_split(apta_node* red, apta_node* blue);
    
    void pre_split(apta_node* red, apta_node* blue);
    void undo_pre_split(apta_node* red, apta_node* blue);

    state_merger();
    state_merger(evaluation_function*, apta*);
    ~state_merger();

    void reset();

    /* performing red-blue merges */
    void perform_merge(apta_node*, apta_node*); // merge function already above
    void undo_perform_merge(apta_node*, apta_node*);
    void perform_split(apta_node*, tail*, int);
    void undo_perform_split(apta_node*, tail*, int);

    /* creating new red states */
    void extend(apta_node* blue);
    void undo_extend(apta_node* blue);

    /* find refinements */
    refinement_set* get_possible_refinements();
    merge_map* get_possible_merges(int);
    refinement* get_best_refinement();
    merge_pair* get_best_merge(int);

    /* find unmergable states */
    apta_node* extend_red(); // what does apta stand for?

    /* update the blue and red states */
    void update_red_blue();

    refinement* test_splits(apta_node* blue);
    refinement* test_merge(apta_node*,apta_node*);
    refinement* test_local_merge(apta_node* red, apta_node* blue);

    state_set* get_all_states();
    state_set* get_red_states();
    state_set* get_blue_states();
    state_set* get_candidate_states();
    state_set* get_sink_states();
    state_set* get_non_sink_states();

    int get_final_apta_size();
    int get_num_red_states();
    int get_num_red_transitions();

    void todot();
    void tojson();
    void print_dot(FILE*);
    void print_json(FILE*);
    void read_apta(istream &input_stream);
    void read_apta(string dfa_file);
//    void read_apta(FILE* dfa_file);
//    void read_apta(boost::python::list dfa_data);
    void read_apta(vector<string> dfa_data);
 // streaming mode methods
    void init_apta(string data);
    void advance_apta(string data);

    int sink_type(apta_node* node);
    bool sink_consistent(apta_node* node, int type);
    int num_sink_types();

    int compute_global_score();

    string dot_output;
    string json_output;

    void store_merge(bool merge_consistent, double merge_score, apta_node *left, apta_node *right);
    refinement* get_stored_merge(apta_node *left, apta_node *right);

    void tojsonsinks();

    refinement *test_split(apta_node *red, tail *t, int attr);

    apta_node *get_state_from_tail(tail *t);

    tail *get_tail_from_state(apta_node *n);

    void undo_split_init(apta_node *red, tail *t, int attr);

    bool split_init(apta_node *red, tail *t, int attr, int depth, bool evaluate, bool perform, bool test);

    bool split(apta_node *new_node, apta_node *old_node, int depth, bool evaluate, bool perform, bool test); // another split?
    void print_dot(string file_name);

    void print_json(string file_name);
};


#endif /* _STATE_MERGER_H_ */
