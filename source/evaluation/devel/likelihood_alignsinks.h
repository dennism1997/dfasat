#ifndef __LLAS__
#define __LLAS__

#include "evaluate.h"
#include "likelihood.h"

/* The data contained in every node of the prefix tree or DFA */
class llas_data: public likelihood_data {
protected:
  REGISTER_DEC_DATATYPE(llas_data);
};

class llas: public likelihoodratio{

protected:
  REGISTER_DEC_TYPE(llas);

public:

    virtual bool compute_consistency(state_merger *merger, apta_node* left, apta_node* right);
    virtual double  compute_score(state_merger*, apta_node* left, apta_node* right);
};

#endif
