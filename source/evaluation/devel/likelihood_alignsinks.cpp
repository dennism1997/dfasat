#include <stdlib.h>
#include <math.h>
#include <vector>
#include <map>
#include <stdio.h>

#include "state_merger.h"
#include "evaluate.h"
#include "depth-driven.h"
#include "likelihood_alignsinks.h"
#include "parameters.h"

REGISTER_DEF_DATATYPE(llas_data);
REGISTER_DEF_TYPE(llas);

typedef pair<apta_node*, tail*> nt_pair;
typedef pair<int, >

struct cmp {
    bool operator()(const pair <int, pair<apta_node*, tail*> > &a, const pair <int, pair<apta_node*, tail*> > &b) {
        return a.first > b.first;
    };
};

bool llas::compute_consistency(state_merger* merger, apta_node* left, apta_node* right) {
    if(right->data->sink_type(right) != -1){
        return true;
    }
    return likelihoodratio::compute_consistency(merger, left, right);
};

double compute_alignment(apta_node* n, tail* t){
    if(t->get_index() == -1) return 0.0;

    double result = t->get_length() - t->get_index() + 1;

    map<apta_node*, map< int, int >> current_counts;

    priority_queue< pair <int, pair<apta_node*, tail*> >, vector< pair <int, pair<apta_node*, tail*> > >, cmp> pq;
    pq.push(pair <int, pair<apta_node*, tail*> >(0, pair<apta_node*, tail*>(n,t)));
    set<apta_node*> encountered_states;
    while(!pq.empty()){
        pair <int, pair<apta_node*, tail*> > next_item = pq.top();
        pq.pop();
        int current_alignment = next_item.first;
        apta_node* current_node = next_item.second.first;
        tail* current_tail = next_item.second.second;
        apta_node* target = current_node->child(current_tail);

        if(current_counts.find(current_node) != current_counts.end() &&
            current_counts[current_node].find(current_tail->get_index()) != current_counts[current_node].end() &&
            current_counts[current_node][current_tail->get_index()] <= current_alignment) continue;
        current_counts[current_node][current_tail->get_index()] = current_alignment;

        if(current_tail->get_index() == -1){
            if(result == -1.0 || result < current_alignment){
                result = current_alignment;
            }
            continue;
        }

        if(result != -1 && current_alignment >= result){
            break;
        }

        //cerr << current_alignment << " " << current_tail << " " << current_node << " " << current_tail->get_index() << " " << current_node->depth <<  endl;

        encountered_states.insert(current_node);

        bool found = false;

        for(guard_map::iterator it = current_node->guards.begin(); it != current_node->guards.end(); ++it) {
            apta_node* child = it->second->target;
            if(child == 0) continue;
            if(child->size == 1 && (*tail_iterator(child)) == current_tail) continue;
            found = true;
            child = child->find();
            if(target == child){
                pq.push(pair <int, pair<apta_node*, tail*> >(current_alignment, pair<apta_node*, tail*>(child,current_tail->future())));
            } else {
                pq.push(pair <int, pair<apta_node*, tail*> >(current_alignment + (current_tail->get_length() - current_tail->get_index()), pair<apta_node*, tail*>(child,current_tail)));
            }
        }
        if(!found){
            if(result == -1.0 || result < current_alignment){
                result = current_alignment;
            }
            continue;
        }
        pq.push(pair <int, pair<apta_node*, tail*> >(current_alignment + (current_tail->get_length() - current_tail->get_index()), pair<apta_node*, tail*>(current_node,current_tail->future())));
        /*for(set<apta_node*>::iterator it = encountered_states.begin(); it != encountered_states.end(); ++it){
            pq.push(pair <int, pair<apta_node*, tail*> >(current_alignment + 1, pair<apta_node*, tail*>(*it,current_tail->future())));
        }*/
    }
    return result;
};

double llas::compute_score(state_merger* merger, apta_node* left, apta_node* right){
    if(right->data->sink_type(right) != -1){
        double min_alignment = 0.0;
        double total_length = 0.0;
        for(tail_iterator it = tail_iterator(right); *it != 0; ++it) {
            tail* t = *it;
            if(t->get_index() != -1){
                min_alignment += t->get_length() - t->get_index() - compute_alignment(left, t);
                total_length += t->get_length() - t->get_index();
            }
        }
        if(total_length == 0) return 0.0;
        min_alignment = min_alignment / total_length;
        return min_alignment;
    }
    return likelihoodratio::compute_score(merger, left, right);
};